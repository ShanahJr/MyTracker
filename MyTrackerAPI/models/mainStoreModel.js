const mongoose = require("mongoose");

const mainStoreSchema = mongoose.Schema({
  mainStoreName: { type: String, required: true },
  imageTitle: { type: String, required: true },
  mainStoreLogo: { type: String, required: true },
  userId: { type: mongoose.Schema.Types.ObjectId, ref: "User", required: true }
});

module.exports = mongoose.model("MainStore", mainStoreSchema);
