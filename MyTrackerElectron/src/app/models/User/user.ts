export class User {
  constructor() { }

  firstName: string;
  lastName: string;
  displayName: string;
  cellNumber: string;
  password: string;
  email: string;
}
