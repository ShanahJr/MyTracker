import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { from, Observable } from "rxjs";
import { User } from "src/app/models/User/user";
import { Login } from "src/app/models/Login/login";
import { Router } from "@angular/router";

@Injectable({
  providedIn: "root",
})
export class UserService {
  public RootUrl: string;
  public httpOptions: any;

  public UserID: number;

  constructor(private http: HttpClient, private router: Router) {
    this.RootUrl = "http://localhost:3000/api/user/";
    this.httpOptions = {
      headers: new HttpHeaders({ "Content-Type": "application/json" }),
    };
  }

  RegisterUser(user: User) {

    return this.http.post<User>(this.RootUrl + 'register', user)
      .subscribe((res) => {
        debugger;

        console.log('I think I am successful');
        this.router.navigate(['/login']);

      });
  }

  Login(Login: Login) {
    return this.http.post<any>(this.RootUrl + "login", Login);
  }
}
