import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Router } from '@angular/router';
@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private http: HttpClient, private router: Router) { }

  GetAuthCode(){

    return this.http.get("https://api.trakt.tv/oauth/authorize?response_type=code&client_id=%20f5f70bbd02d74b6c4a68df19f65ff6dfa5880c5c6ee8b9bd34dcb711a6cf8eba&redirect_uri=http%3A%2F%2Flocalhost%3A8100")

  }

}
