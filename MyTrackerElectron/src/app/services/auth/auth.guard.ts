import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import * as JwtDecode from 'jwt-decode';
import { JwtHelperService } from '@auth0/angular-jwt';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  constructor(
    private router: Router,
  ) { }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): boolean {
    if (localStorage.getItem("UserToken") == null) {
      this.router.navigate(['/login']);
    }
    else {

      //Get the role from the route you are trying to access
      const role = next.data.role as string;

      //Get user token from local storage
      const userToken = localStorage.getItem("UserToken");

      //initialize the token decoder
      const helper = new JwtHelperService();

      //Decode the user token to get the user role
      const decodedUserToken = helper.decodeToken(userToken);

      if (helper.isTokenExpired(userToken)) {
        debugger;
        this.router.navigate(['/login']);
      }

      //Get user role from the decoeded token
      const userRole = decodedUserToken.userRole;

      // if (role == userRole) {
      //   return true
      // }
      // else {
      //   debugger;
      //   this.router.navigate(['/login']);
      // }
      return true;

    }
    return true;

  }
}
