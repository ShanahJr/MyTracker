import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ShowTrackerPage } from './show-tracker.page';

const routes: Routes = [
  {
    path: '',
    component: ShowTrackerPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ShowTrackerPageRoutingModule {}
