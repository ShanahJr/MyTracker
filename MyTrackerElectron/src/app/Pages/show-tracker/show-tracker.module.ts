import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ShowTrackerPageRoutingModule } from './show-tracker-routing.module';

import { ShowTrackerPage } from './show-tracker.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ShowTrackerPageRoutingModule
  ],
  declarations: [ShowTrackerPage]
})
export class ShowTrackerPageModule {}
