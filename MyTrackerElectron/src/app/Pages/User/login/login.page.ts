import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { take } from 'rxjs/operators';
import { UserService } from 'src/app/services/user/user.service';
import { Login } from 'src/app/models/Login/login';
import { AuthService } from 'src/app/services/auth/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  LoginForm: any;
  errorMessage: string;

  constructor(
    private userService: UserService, 
    private router: Router,
    private route: ActivatedRoute,
    private traktAuth : AuthService
    ) { }

  ngOnInit() {
    // this.LoginForm = new FormGroup({
    //   password: new FormControl("", [Validators.required]),
    //   email: new FormControl("", [Validators.required, Validators.email]),
    // });

    this.route.queryParams.subscribe( (params) => {
      if (params.code) {
        console.log(params.code);
      }else{
        window.open('https://trakt.tv/oauth/authorize?client_id=f5f70bbd02d74b6c4a68df19f65ff6dfa5880c5c6ee8b9bd34dcb711a6cf8eba&redirect_uri=http%3A%2F%2Flocalhost%3A8100%2Flogin&response_type=code','_blank')
        console.log("Window has closed");
        console.log(params.code);
      }

    })

    
    // this.traktAuth.GetAuthCode().subscribe((res) => {



    // })

  }

  LogIn() {
    var Login: Login = this.LoginForm.value;
    this.userService
      .Login(Login)
      .pipe(take(1))
      .subscribe((res) => {
        localStorage.setItem("UserToken", res.token);
        this.router.navigate(["/tabs"]);
        //this.userService.UserID = res.userID;
      });
  }

}
