import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from 'src/app/services/auth/auth.guard';

import { TabsPage } from './tabs.page';

const routes: Routes = [
  {
    path: 'tabs',
    component: TabsPage,
    children: [

      {
        path: 'ShowTracker',
        loadChildren: () => import("../show-tracker/show-tracker.module").then(m => m.ShowTrackerPageModule),
        canActivate: [AuthGuard]
      },
      {
        path: 'ActivityTracker',
        loadChildren: () => import("../activity-tracker/activity-tracker.module").then(m => m.ActivityTrackerPageModule),
        canActivate: [AuthGuard]
      },
      {
        path: 'Notifications',
        loadChildren: () => import("../notifications/notifications.module").then(m => m.NotificationsPageModule),
        canActivate: [AuthGuard]
      },
      {
        path: '',
        redirectTo: '/tabs/ShowTracker',
        pathMatch: 'full'
      }
    ]
  }
  ,
  {
    path: '',
    redirectTo: '/tabs/ShowTracker',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TabsPageRoutingModule { }
